from copy import deepcopy
import io
import os
import pytest
from workflow_utils.artifact import (
    Artifact,
    FsArtifactSource, MavenArtifactSource,
    FsArtifactCache, FsMavenArtifactCache
)


@pytest.fixture(name='local_maven_repo')
def fixture_local_maven_repo(tmpdir):
    """
    Yields a path to a tmpdir with a maven artifact jar in it
    """
    maven_repo_path = os.path.join(tmpdir, 'local_maven_repo')
    a1_dir = os.path.join(maven_repo_path, 'org/apache/hadoop/hadoop-yarn-client/2.10.1')
    a1_file = os.path.join(a1_dir, 'hadoop-yarn-client-2.10.1.jar')
    os.makedirs(a1_dir)
    with open(a1_file, 'wt', encoding="utf-8") as file:
        file.write("FAKE JAR")
    yield maven_repo_path


@pytest.fixture(name='fs_artifact_source')
def fixture_fs_artifact_source(local_maven_repo):
    """
    Yields a path to a tmpdir with a maven artifact jar in it and
    an instantiated FsArtifactSource
    """
    yield FsArtifactSource(local_maven_repo)


@pytest.fixture(name='maven_artifact_source')
def fixture_maven_artifact_source(local_maven_repo):
    """
    Yields an instantiated MavenArtifactSource using
    a local maven tmpdir repo with an artifact in it.
    """
    yield MavenArtifactSource(local_maven_repo)


@pytest.fixture(name='fs_artifact_cache')
def fixture_fs_artifact_cache(tmpdir):
    """
    Yields a FsArtifactCache with one item in the cache.
    """

    local_cache_dir = os.path.join(tmpdir, 'artifact_cache_dir')
    a1_file = os.path.join(
        local_cache_dir,
        'org_apache_hadoop_hadoop-yarn-client_2.10.1_hadoop-yarn-client-2.10.1.jar'
    )
    os.makedirs(local_cache_dir)
    with open(a1_file, 'wt', encoding="utf-8") as file:
        file.write("FAKE JAR")

    yield FsArtifactCache(local_cache_dir)


@pytest.fixture(name='fs_maven_artifact_cache')
def fixture_fs_maven_artifact_cache(tmpdir):
    """
    Yields a FsArtifactCache with one item in the cache.
    """

    local_cache_dir = os.path.join(tmpdir, 'maven_artifact_cache_dir')
    a1_file = os.path.join(
        local_cache_dir,
        'org_apache_hadoop_hadoop-yarn-client_2.10.1_hadoop-yarn-client-2.10.1.jar'
    )
    os.makedirs(local_cache_dir)
    with open(a1_file, 'wt', encoding="utf-8") as file:
        file.write("FAKE JAR")

    yield FsMavenArtifactCache(local_cache_dir)


@pytest.fixture(name='artifact')
def fixture_artifact(maven_artifact_source, tmpdir):
    local_cache_dir = os.path.join(tmpdir, 'artifact_cache_dir_0')
    yield Artifact(
        id='org.apache.hadoop:hadoop-yarn-client:2.10.1',
        source=maven_artifact_source,
        # Use a new empty cache
        caches=[FsMavenArtifactCache(local_cache_dir)],
        name='hadoop-yarn-client-2.10.1.jar'
    )


@pytest.fixture(name='artifact_configs')
def fixture_artifact_configs(
    fs_artifact_source,
    maven_artifact_source,
    fs_artifact_cache,
    fs_maven_artifact_cache
):
    yield {
        'artifacts': {
            'hadoop-yarn-client-2.10.1.jar': {
                'id': 'org.apache.hadoop:hadoop-yarn-client:2.10.1',
                'source': 'maven_source',
                'caches': ['maven_cache'],
            },
            'my_artifact.zip': {
                'id': 'my_artifact.zip',
            },
        },
        'artifact_sources': {
            'fs_source': {
                'class_name': 'workflow_utils.artifact.FsArtifactSource',
                'base_uri': fs_artifact_source.base_uri
            },
            'maven_source': {
                'class_name': 'workflow_utils.artifact.MavenArtifactSource',
                'base_uri': maven_artifact_source.base_uri
            },
        },
        'artifact_caches': {
            'fs_cache': {
                'class_name': 'workflow_utils.artifact.FsArtifactCache',
                'base_uri': fs_artifact_cache.base_uri
            },
            'maven_cache': {
                'class_name': 'workflow_utils.artifact.FsMavenArtifactCache',
                'base_uri': fs_maven_artifact_cache.base_uri
            },
        },
        'default_artifact_source': 'fs_source',
        'default_artifact_caches': ['fs_cache']
    }


# == Test ArtifactSources ==
def test_fs_artifact_source_url(fs_artifact_source):
    """
    FsArtifactSource.url() should return the artifact id
    prefixed with the base_uri.
    """
    artifact_id = 'org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar'
    expected = os.path.join(fs_artifact_source.base_uri, artifact_id)
    assert fs_artifact_source.url(artifact_id) == expected


def test_fs_artifact_source_exists(fs_artifact_source):
    artifact_id = 'org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar'
    assert fs_artifact_source.exists(artifact_id)
    assert not fs_artifact_source.exists('not/exists')


def test_fs_artifact_source_open(fs_artifact_source):
    artifact_id = 'org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar'
    with fs_artifact_source.open(artifact_id) as file:
        assert file.read() == b'FAKE JAR'


def test_maven_artifact_source_url(maven_artifact_source):
    """
    MavenArtifactSource.url() should translate from a maven coordinate to a URL.
    """
    artifact_id = 'org.apache.hadoop:hadoop-yarn-client:2.10.1'
    expected = os.path.join(
        maven_artifact_source.base_uri,
        'org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar'
    )
    assert maven_artifact_source.url(artifact_id) == expected


# == Test ArtifactCaches ==
def test_fs_artifact_cache_cache_key(fs_artifact_cache):
    """
    FsArtifactCache.url() should just return the
    a safe filename of id.
    """
    artifact_id = 'org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar'
    expected = 'org_apache_hadoop_hadoop-yarn-client_2.10.1_hadoop-yarn-client-2.10.1.jar'
    assert fs_artifact_cache.cache_key(artifact_id) == expected


def test_fs_artifact_cache_url(fs_artifact_cache):
    """
    FsArtifactCache.url() should just return the
    a safe filename of id prefixed with the cache base uri.
    """
    artifact_id = 'org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar'
    expected = os.path.join(
        fs_artifact_cache.base_uri,
        'org_apache_hadoop_hadoop-yarn-client_2.10.1_hadoop-yarn-client-2.10.1.jar'
    )
    assert fs_artifact_cache.url(artifact_id) == expected


def test_fs_artifact_cache_exists(fs_artifact_cache):
    artifact_id = 'org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar'
    assert fs_artifact_cache.exists(artifact_id)
    assert not fs_artifact_cache.exists('not/exists')


def test_fs_artifact_cache_put(fs_artifact_cache):
    """
    Test that a put of new content will result in the content
    being written into the cache at the cache url for the artifact id.
    """
    artifact_id = 'org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar'

    # Test source artifact input
    content = io.BytesIO(b'NEW FAKE JAR')
    fs_artifact_cache.put(artifact_id, content, force=True)
    content.close()

    with open(fs_artifact_cache.url(artifact_id), mode='rb') as file:
        assert file.read() == b'NEW FAKE JAR'


def test_fs_artifact_cache_delete(fs_artifact_cache):
    artifact_id = 'org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar'
    fs_artifact_cache.delete(artifact_id)
    assert not fs_artifact_cache.exists(artifact_id)


def test_fs_maven_artifact_cache_url(fs_maven_artifact_cache):
    """
    FsMavenArtifactCache.url() should just return the
    path in a maven repo for a maven coordinate artifact id.
    """
    artifact_id = 'org.apache.hadoop:hadoop-yarn-client:2.10.1'
    expected = os.path.join(
        fs_maven_artifact_cache.base_uri,
        'org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar'
    )
    assert fs_maven_artifact_cache.url(artifact_id) == expected


# == Test Artifact ==
def test_artifact_locators(artifact):
    assert len(artifact.locators(must_exist=False)) == 2, \
        "artifact has a source and one cache, so # of locators should be 2"
    assert len(artifact.locators(must_exist=True)) == 1, \
        "artifact has not yet been cached, so # of locatators with must_exist should be 1"


def test_artifact_exists(artifact):
    assert artifact.exists() is True


def test_artifact_exists_in_cache(artifact):
    assert artifact.exists_in_cache() is False


def test_artifact_urls(artifact):
    assert len(artifact.urls(must_exist=False)) == 2, \
        "artifact has a source and one cache, so # of urls should be 2"
    assert len(artifact.urls(must_exist=True)) == 1, \
        "artifact has not yet been cached, so # of urls with must_exist should be 1"


def test_artifact_cached_urls(artifact):
    assert len(artifact.cached_urls()) == 0, \
        "artifact has not yet been cached, so # of urls in caches should be 0"


def test_artifact_cached_url(artifact):
    assert artifact.cached_url() is None, \
        "artifact has not yet been cached, so cached url should be None"


def test_artifact_cache_put_and_delete(artifact):
    artifact.cache_put()
    assert len(artifact.locators(must_exist=True)) == 2, \
        "artifact has been cached, so # of locatators with must_exist should be 2"

    artifact.cache_delete()
    assert len(artifact.locators(must_exist=True)) == 1, \
        "artifact has been deleted from cache, so # of locatators with must_exist should be 1"


def test_artifact_factory(
    fs_artifact_source,
    maven_artifact_source,
    fs_artifact_cache,
    fs_maven_artifact_cache
):
    available_sources = {
        'fs_source': fs_artifact_source,
        'maven_source': maven_artifact_source,
    }
    available_caches = {
        'fs_cache': fs_artifact_cache,
        'maven_cache': fs_maven_artifact_cache,
    }
    default_source_name = 'fs_source'
    default_cache_names = ['fs_cache']

    maven_artifact_config = {
        'id': 'org.apache.hadoop:hadoop-yarn-client:2.10.1',
        'source': 'maven_source',
        'caches': ['maven_cache']
    }
    maven_artifact = Artifact.factory(
        maven_artifact_config,
        available_sources,
        available_caches,
        default_source_name,
        default_cache_names,
        name='hadoop-yarn-client-2.10.1.jar',
    )
    assert maven_artifact.urls(must_exist=False) == [
        fs_maven_artifact_cache.url(maven_artifact_config['id']),
        maven_artifact_source.url(maven_artifact_config['id']),
    ], "Should instantiate an Artifact from config"

    # Since we provided default source and caches,
    # all this artifact needs is an id.
    fs_artifact_config = {
        'id': 'my_artifact.zip'
    }
    fs_artifact = Artifact.factory(
        fs_artifact_config,
        available_sources,
        available_caches,
        default_source_name,
        default_cache_names,
        # Don't strictly need a name, it will default to id.
    )
    assert fs_artifact.urls(must_exist=False) == [
        fs_artifact_cache.url(fs_artifact_config['id']),
        fs_artifact_source.url(fs_artifact_config['id']),
    ], "Should instantiate an Artifact from config with defaults"


def test_load_artifacts_from_config(
    artifact_configs,
    fs_artifact_source,
    maven_artifact_source,
    fs_artifact_cache,
    fs_maven_artifact_cache
):
    artifacts = Artifact.load_artifacts_from_config(artifact_configs)

    maven_artifact = artifacts['hadoop-yarn-client-2.10.1.jar']
    maven_artifact_id = artifact_configs['artifacts'][
        'hadoop-yarn-client-2.10.1.jar'
    ]['id']
    assert maven_artifact.id == maven_artifact_id
    assert maven_artifact.urls(must_exist=False) == [
        fs_maven_artifact_cache.url(maven_artifact_id),
        maven_artifact_source.url(maven_artifact_id),
    ]

    fs_artifact = artifacts['my_artifact.zip']
    fs_artifact_id = artifact_configs['artifacts']['my_artifact.zip']['id']
    assert fs_artifact.id == fs_artifact_id
    assert fs_artifact.urls(must_exist=False) == [
        fs_artifact_cache.url(fs_artifact_id),
        fs_artifact_source.url(fs_artifact_id),
    ]


def test_load_artifacts_from_config_with_incorrect_config(
    artifact_configs
):
    artifact_configs_for_test = deepcopy(artifact_configs)
    del artifact_configs_for_test['artifacts']['my_artifact.zip']['id']
    # ValueError, id is required
    with pytest.raises(ValueError):
        Artifact.load_artifacts_from_config(artifact_configs_for_test)

    artifact_configs_for_test = deepcopy(artifact_configs)
    del artifact_configs_for_test['default_artifact_source']
    # ValueError, source is required if no default is provided
    with pytest.raises(ValueError):
        Artifact.load_artifacts_from_config(artifact_configs_for_test)

    artifact_configs_for_test = deepcopy(artifact_configs)
    del artifact_configs_for_test['artifact_sources']['maven_source']
    # KeyError, since maven_source is not defined, but the
    # hadoop-yarn-client-2.10.1.jar artifact references it.
    with pytest.raises(KeyError):
        Artifact.load_artifacts_from_config(artifact_configs_for_test)

    artifact_configs_for_test = deepcopy(artifact_configs)
    del artifact_configs_for_test['artifact_sources']
    # ValueError, because now artifact_sources are not defined
    # at all, but it must be.
    with pytest.raises(ValueError):
        Artifact.load_artifacts_from_config(artifact_configs_for_test)
