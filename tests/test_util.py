import pytest
from workflow_utils.util import (
    safe_filename,
    instantiate,
)


@pytest.mark.parametrize('name, expected', [
    ("a/b/c", "a_b_c"),
    ("d.e-f", "d.e-f"),
    ("org.apache.hadoop:hadoop-yarn-client:2.10.1", "org.apache.hadoop_hadoop-yarn-client_2.10.1"),
])
def test_safe_filename(name, expected):
    assert safe_filename(name) == expected


class MyClass():  # pylint: disable=too-few-public-methods
    def __init__(self, name):
        self.name = name


def test_instantiate():
    instance = instantiate(
        class_name='tests.test_util.MyClass',
        name='a1'
    )

    assert isinstance(instance, MyClass), \
        'instantiate should work with class_name string as kwarg'
    assert instance.name == 'a1'

    instance = instantiate(**{
        'class_name': 'tests.test_util.MyClass',
        'name': 'a2'
    })
    assert isinstance(instance, MyClass), \
        'instantiate should work with class_name string in kwargs dict'
    assert instance.name == 'a2'

    with pytest.raises(ValueError):
        instantiate(
            **{'class_name': 'NotDefinedClass', 'name': 'a3'}
        )

    with pytest.raises(AttributeError):
        instantiate(
            **{'class_name': 'tests.test_util.NotDefinedClass', 'name': 'a3'}
        )
