from typing import List

import logging
import importlib
import os


class LogHelper():  # pylint: disable=too-few-public-methods
    """
    Inherit from this class (multi-inheritance is fine) to get
    a self.log Logger named after the fully qualified class name
    with 'instance_repr' entry for use by logging formatters.
    instance_repr is populated by calling __repr__, which
    is implemented here to just return the class name.

    Usage:
    class MyClass(LogHelper):

        def my_method(self):
            self.log.info("my message")

    """
    def __init__(self):
        """
        Creates a class instance specific logger as self.log using __repr__.
        Your implementing class should probably call
        super().__init__() from its __init__ method to get this.
        """
        # Use a logger that is named module.ClassName, but has an extra
        # entry in the logging record instance_repr == self.__repr__().
        self.log = logging.LoggerAdapter(
            logging.getLogger(f'{self.__module__}.{self.__class__.__name__}'),
            {'instance_repr': self.__repr__()}
        )

    def __repr__(self):
        return self.__class__.__name__


def safe_filename(name: str, maxlen: int = 255):
    """
    Given a string, returns a 'safe' filename.

    :param name:
    :return: safe filename
    """
    keep_chars = ['.', '_', '-']

    def safe_char(char):
        if char.isalnum() or char in keep_chars:
            return char
        return "_"

    return "".join(safe_char(c) for c in name).rstrip("_")[0:maxlen]


def filter_files_exist(files: List[str]) -> List[str]:
    return [file for file in files if os.path.exists(file)]


def instantiate(class_name: str, **args):
    """
    Factory method for instantiating classes from
    kwargs.  class_name is expected to be a fully qualified class name of a
    python class that is importable in PYTHONPATH.

    :param class_name: a fully qualified class name string
    :param **args: These are passed to the class constructor.

    Example:
    ::
        $ tree /path/to/code
        /path/to/code/
        --- ext
            |-- myclass.py
        $ head -n 5 /path/to/code/ext/myclass.py
        class MyClass(object):
            def __init__(name):
                self.name = name
                super().__init__()
        $ export PYTHONPATH=/path/to/code
        ...
        >>> instantiate(type='ext.myclass.MyClass', **{name: 'my_name'})
    """
    if '.' not in class_name:
        raise ValueError(
            f'Cannot import class from {class_name}, it is not in'
            'a fully qualified \'module.ClassName\' format.'
        )
    module_name, symbol_name = class_name.rsplit('.', 1)
    module = importlib.import_module(module_name)
    cls = getattr(module, symbol_name)

    return cls(**args)


def instantiate_all(instances_config):
    """
    Given a dict of instance config keyed by name,
    call instantiate() on each of them, and return
    a new dict that keys the name to the instance.

    instances_config must be a dict that looks like:

    instance_name1:
        class_name: my.module.ClassName
        arg1: abc
        arg2: 342
    instance_name2:
        class_name: my.other.ClassName
    # ...

    """
    return {
        name: instantiate(**args)
        for name, args in instances_config.items()
    }


# NOTE: i dunno about this.
def setup_logging(level=None):
    """
    Configures basic logging defaults.
    If level is not given, but the environment variable LOG_LEVEL
    is set, it will be used as the level.  Otherwise INFO is the default level.

    An instance_repr entry will be added to every log record if one
    doesn't already exist.  This allows us to use formatters
    that refer to %(instance_repr).  LogHelper instances
    always add an instance_repr entry to the log record.

    Args:
        level (str): log level
    """
    if level is None:
        level = getattr(
            logging, os.environ.get('LOG_LEVEL', 'INFO')
        )

    logging.basicConfig(
        level=level,
        format='%(asctime)s %(levelname)-8s %(instance_repr)-40s %(message)s',
        datefmt='%Y-%m-%dT%H:%M:%S%z',
    )

    logging.getLogger('workflow_utils.yamlreader').setLevel(logging.INFO)
    logging.getLogger('conda').setLevel(logging.INFO)

    # Since we use instance_repr in the format of the basic logger,
    # All log records also need to have an instance_repr entry.
    # Add a logging filter to the root logger's handlers to
    # make sure that instance_repr is set if a child logger hasn't set it.
    def inject_instance_name(record):
        if not hasattr(record, 'instance_repr'):
            record.instance_repr = record.name
        return True

    root_logger = logging.getLogger()
    for handler in root_logger.handlers:
        handler.addFilter(inject_instance_name)
