import os
import sys
from docopt import docopt  # type: ignore

from workflow_utils.artifact import Artifact
from workflow_utils.util import setup_logging


def main(argv=sys.argv[1:]):  # pylint: disable=dangerous-default-value
    script_name = os.path.basename(sys.argv[0])
    doc = f"""
    Usage: {script_name} status <artifact_config_files>...
           {script_name} warm [--force] <artifact_config_files>...
           {script_name} clear <artifact_config_files>...
    """
    main.__doc__ = doc

    setup_logging()
    args = docopt(main.__doc__, argv)
    artifacts = Artifact.load_artifacts_from_config(args['<artifact_config_files>'])

    if args['warm']:
        for artifact in artifacts.values():
            artifact.cache_put(force=args['--force'])
    elif args['clear']:
        for artifact in artifacts.values():
            artifact.cache_delete()

    for artifact in artifacts.values():
        print(artifact)


if __name__ == '__main__':
    main(sys.argv[1:])
