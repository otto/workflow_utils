from typing import List, Sequence, Optional
from yamlreader import yaml_load  # type: ignore

from workflow_utils.artifact.locator import ArtifactLocator
from workflow_utils.artifact.source import ArtifactSource
from workflow_utils.artifact.cache import ArtifactCache
from workflow_utils.util import instantiate_all


class Artifact():
    """
    An Artifact is identified by 'id', and
    has a source URL and a list of cached URLs.
    These URLs are looked up by an ArtifactSource locator
    and ArtifactCache locators.

    This class exists to do the following:
        - Locate artifacts in sources
        - Cache those artifacts in filesystem caches.
    """
    def __init__(
        self,
        id: str,  # pylint: disable=redefined-builtin,invalid-name
        source: ArtifactSource,
        caches: Optional[List[ArtifactCache]] = None,
        name: Optional[str] = None,
    ):
        """
        :param id:
            This will be used to locate the artifact.
            This is often a URL.

        :param source:
            ArtifactSource

        :param caches:
            List of ArtifactCache where the artifact can be cached.
            Default: []

        :param name:
            a logical name of this artifact.  Defaults to id if not provided.
        """
        self.id = id  # pylint: disable=invalid-name
        self.name = name or id
        self.source = source
        self.caches = caches or []

        super().__init__()

    def __repr__(self):
        return f'{self.__class__.__name__}({self.name})'

    def __str__(self):
        locators_desc = [
            f'{locator.url(self.id)}\t(exists={locator.exists(self.id)})'
            for locator in self.locators()
        ]
        return self.__repr__() + ":\n\t" + "\n\t".join(locators_desc)

    def cache_put(self, force: bool = False):
        """
        Puts the artifact in all of its cache locations.
        """
        for cache in self.caches:
            cache.put(self.id, self.source.open(self.id), force=force)

    def cache_delete(self):
        """
        Deletes the artifact from all of its cache locations.
        """
        for cache in self.caches:
            if cache.exists(self.id):
                cache.delete(self.id)

    def locators(self, must_exist: bool = False) -> Sequence[ArtifactLocator]:
        """
        Returns a list of all AritfactLocators for this artifact,
        including ArtifactCaches. The ArtifactSource locator is the
        last in the list.

        :param must_exist: Only return ArtifactLocators where the artifact exists.
        """
        return [
            locator for locator in self.caches + [self.source]  # type: ignore
            if locator.exists(self.id) or not must_exist
        ]

    def exists(self) -> bool:
        """
        Returns true if this artifact exists at any of its
        source or cached locations.
        """
        return len(self.locators(must_exist=True)) > 0

    def exists_in_cache(self) -> bool:
        """
        Returns true if this artifact exists at any of its
        cached locations.
        """
        return len(self.cached_urls()) > 0

    def urls(self, must_exist: bool = False) -> List[str]:
        """
        Returns a list of all artifact URLs, including caches.
        The source URL is the last in the list.

        :param must_exist: Only return URLs where the artifact exists.
        """
        return [
            locator.url(self.id)
            for locator in self.locators(must_exist=must_exist)
        ]

    def url(self, must_exist: bool = False) -> Optional[str]:
        """
        Returns the first URL of the artifact.
        Cached URLs will be preferred over the source URL.
        If no URLs have existent artifacts, returns None.

        :param must_exist: Only return a location that has the artifact.
        """
        urls = self.urls(must_exist=must_exist)
        if len(urls) > 0:
            return urls[0]
        return None

    def cached_urls(self) -> List[str]:
        """
        Returns cached URLs for this artifact that exist.
        """
        return [
            cache.url(self.id) for cache in self.caches
            if cache.exists(self.id)
        ]

    def cached_url(self) -> Optional[str]:
        """
        Returns the first cached URL for this artifact that exists,
        or None if no cached URL exists.
        """
        urls = [
            cache.url(self.id) for cache in self.caches
            if cache.exists(self.id)
        ]
        if len(urls) > 0:
            return urls[0]
        return None

    @classmethod
    def factory(
        cls,
        artifact_config: dict,
        available_sources: dict,
        available_caches: dict,
        default_source_name: Optional[str] = None,
        default_cache_names: Optional[List[str]] = None,
        name: Optional[str] = None,
    ):
        """
        Instantiates an Artifact from a config dict.

        In the artifact_config, sources and caches
        are referred to by names, which should be
        entries in the available_sources and
        available_caches dicts.

        :param artifact_config:
            dict of artifact config. This should contain
            an id, and source and caches by name as keys
            into the available_sources and available_caches
            dicts.

        :param available_sources:
            dict of available ArtifactSources, keyed by name.

        :param available_caches:
            dict of available ArtifactCaches, keyed by name.

        :param default_source_name:
            ArtifactSource name (entry in available_sources)
            to use if the artifact_config does not specify source.

        :param default_cache_names:
            List of ArtifactCache names (entries in available_caches)
            to use if the artifact_config does not specify caches.
            Default: [] (no caches).

        :param name:
            artifact name

        """
        name_for_errors = name or artifact_config.get('id') or ''
        if 'id' not in artifact_config:
            raise ValueError(
                f'`id` was not defined in artifact_config for artifact \'{name_for_errors}\''
            )

        if 'source' in artifact_config:
            source_name = artifact_config['source']
        elif default_source_name is not None:
            source_name = default_source_name
        else:
            raise ValueError(
                f'`source` was not defined in artifact_config for artifact \'{name_for_errors}\''
            )

        # This artifact uses the source called source_name.
        # Raises KeyError if not defined.
        source = available_sources[source_name]

        if 'caches' in artifact_config:
            cache_names = artifact_config['caches']
        elif default_cache_names is not None:
            cache_names = default_cache_names
        else:
            # No default cache defined, so use empty list.
            cache_names = []

        # This will raise KeyError if not defined.
        caches = [
            available_caches[cache_name]
            for cache_name in cache_names
        ]

        # Instantiate the Artifact
        return cls(
            id=artifact_config['id'],
            name=name,
            source=source,
            caches=caches
        )

    @classmethod
    def load_artifacts_from_config(cls, config) -> dict:
        """
        Parses the config to instantiate ArtifactSources, ArtifactCaches
        and Artifacts declared with those sources and caches.

        The provided config dict is expected to have the keys
        'artifact_sources', 'artifact_caches', and 'artifacts'.
        'artifact_sources' and 'artifact_caches' are instantiated
        directly by their class name.

        'artifacts' are instantiated using the declared sources and caches.

        :param config:
            Either a dict containing artifact, source and cache config,
            or paths (or anything that yaml_read takes) to yaml config
            files containing this config.

        :return: a dict of Artifacts keyed by name.
        """

        # If a config dict, just use it.
        if isinstance(config, dict):
            pass
        # Else load the config from yaml config files.
        else:
            config = yaml_load(config)

        # Instantiate all ArtifactSources declared in artifact_sources.
        available_sources = {}
        if 'artifact_sources' in config:
            available_sources = instantiate_all(config['artifact_sources'])
        else:
            raise ValueError(f'{__name__}  expected `artifact_sources` entry in config')

        # Instantiate all ArtifactCaches declared in artifact_caches.
        available_caches = {}
        if 'artifact_caches' in config:
            available_caches = instantiate_all(config['artifact_caches'])

        # Instantiate all Artifacts, using available sources and caches.
        if 'artifacts' in config:
            return {
                artifact_name: cls.factory(
                    name=artifact_name,
                    artifact_config=artifact_config,
                    available_sources=available_sources,
                    available_caches=available_caches,
                    default_source_name=config.get('default_artifact_source'),
                    default_cache_names=config.get('default_artifact_caches')
                ) for artifact_name, artifact_config in config['artifacts'].items()
            }

        return {}
