from abc import abstractmethod
import os
import fsspec  # type: ignore

from workflow_utils.util import safe_filename
from workflow_utils.artifact.maven import maven_artifact_uri
from workflow_utils.artifact.locator import ArtifactLocator


class ArtifactCache(ArtifactLocator):
    """
    A Artifact 'cache' Locator..
    An ArtifactCache can open artifacts for writing.

    Cached locations are generally meant to be filesystems of some kind.
    There is rarely a need for a 'cache get' of an Artifact.  Instead,
    we only need to put Artifacts a filesystem backed cache, and use
    allow others to open the URLs to the cached artifacts.
    """

    @classmethod
    def cache_key(cls, artifact_id: str) -> str:
        """
        Key to use in the cache, overrartifact_ide this if you want to
        use a different key string.

        :param artifact_id: artifact_id
        """
        # NOTE: It would be nice to have the resolved artifact
        # source.url(artifact_id) here, rather
        # than just the artifact name. If we had that, we could cache the
        # artifact with the same filename as the source. This will work
        # if the artifact_id IS a URL, in the case of most URI only based sources
        # (like FsArtifactSource). If we use e.g. FsArtifactCache
        # behind a MavenArtifactSource everything will work, but the filenames
        # will just be a 'safe name' of the Maven coordinate like
        # org.apache.hadoop_hadoop-yarn-client_2.10.1, instead
        # of a nice filename like hadoop_hadoop-yarn-client_2.10.1.jar.
        # We could do this if we passed an ArtifactSource to
        # cache.put instead of an file-like input object.
        # But then we'd have to also pass an ArtifactSource
        # to methods like url and cache_key in order to get the
        # 'cache key' filename from the ArtifactSource + artifact_id.
        return safe_filename(artifact_id)

    @abstractmethod
    def open(self, artifact_id: str):
        """
        Returns an file-like object at the cache URL for writing.

        :param artifact_id: artifact_id
        """

    @abstractmethod
    def put(self, artifact_id, input_stream, force=False):
        """
        Put input in cache with artifact name.

        :param artifact_id: artifact_id
        :param input_stream: open file-like object to read input data from
        :param force: If true, input will be put in cache even if it already exists.
        """

    @abstractmethod
    def delete(self, artifact_id: str):
        """
        Delete artifact from cache.

        :param artifact_id: artifact_id
        """


class FsArtifactCache(ArtifactCache):
    """
    ArtifactCache for use with any writable fsspec FileSystem.
    """

    def __init__(self, base_uri: str = ''):
        """
        :param base_uri:
            Base URI in which to store cached artifacts.
            This URI should start with a supported fsspec protocol.
        """
        self.base_uri = base_uri
        super().__init__()

    def __repr__(self):
        return f'{self.__class__.__name__}({self.base_uri})'

    def fs(self) -> fsspec.AbstractFileSystem:  # pylint: disable=invalid-name
        """
        We don't keep a reference to the FileSystem used by
        this name.  The FileSystem can be obtained by calling
        fspec.open, which returns an OpenFile object, which
        has a reference to the FileSystem.
        """
        return fsspec.open(self.base_uri).fs

    def url(self, artifact_id: str) -> str:
        return os.path.join(self.base_uri, self.cache_key(artifact_id))

    def exists(self, artifact_id: str):
        return self.fs().exists(self.url(artifact_id))

    def open(self, artifact_id: str):
        url = self.url(artifact_id)
        self.log.debug(f'Opening {artifact_id} cache {url} for writing.')
        return fsspec.open(url, mode='wb').open()

    def put(self, artifact_id: str, input_stream, force=False):
        if force or not self.exists(artifact_id):
            self.log.debug(f'Cache put of {artifact_id} (force={force})')
            with self.open(artifact_id) as output:
                # Q: is this best way to stream from input -> output file?
                output.write(input_stream.read())
        else:
            self.log.debug(f'{artifact_id} is already cached.')

    def delete(self, artifact_id: str):
        if self.exists(artifact_id):
            self.log.debug(f'Cache delete of {artifact_id}')
            self.fs().rm(self.url(artifact_id))


class FsMavenArtifactCache(FsArtifactCache):
    """
    Caches Maven based artifacts using their coordinates
    in a Maven repository directory hierarchy in a writable file system.
    e.g. an artifact_id coordinate of
    'org.apache.hadoop:hadoop-yarn-client:2.10.1'
    would result in cache key of
    org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar
    """
    @classmethod
    def cache_key(cls, artifact_id: str) -> str:
        """
        Translates a coordinate to a relative maven path,
        allowing Maven artifacts to be cached in
        a Maven compatible directory hierarchy.

        :param artifact_id: Should be a maven coordinate.
        """
        return maven_artifact_uri(artifact_id)
