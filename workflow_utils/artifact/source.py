from abc import abstractmethod
import os
import fsspec  # type: ignore

from workflow_utils.artifact.locator import ArtifactLocator
from workflow_utils.artifact.maven import maven_artifact_uri


class ArtifactSource(ArtifactLocator):
    """
    An ArtifactLocator that can open artifacts for reading.
    """
    @abstractmethod
    def open(self, artifact_id: str):
        """
        Returns read only file-like IO stream object for artifact_id.

        :param artifact_id: artifact_id, implementation specific.
        """


class FsArtifactSource(ArtifactSource):
    """
    Source of artifacts retrievable via URIs.
    All protocols supported by fsspec are supported.
    E.g. local fs, HDFS, HTTP, etc.

    For FsArtifactSource, the artifact_id should be a URI.

    :param base_uri:
        Base URI to prefix to all URI artifact_ids.
        If not provided, it is expected that artifact_ids
        are absolute URLs. If they aren't, an error will occur.
    """

    def __init__(self, base_uri: str = ''):
        self.base_uri = base_uri
        super().__init__()

    def __repr__(self):
        return f'{self.__class__.__name__}({self.base_uri})'

    def url(self, artifact_id: str) -> str:
        """
        For FsArtifactSource, the url is the artifact_id prefixed
        with the source's base_uri.

        :param artifact_id: a URI
        """
        return os.path.join(self.base_uri, artifact_id)

    def open(self, artifact_id: str):
        """
        :param artifact_id: a URI
        """
        url = self.url(artifact_id)
        self.log.debug(f'Opening source {url} for reading.')
        return fsspec.open(url).open()

    def exists(self, artifact_id: str) -> bool:
        """
        :param artifact_id: a URI
        """
        return fsspec.open(
            self.url(artifact_id)
        ).fs.exists(self.url(artifact_id))


class MavenArtifactSource(FsArtifactSource):
    """
    Source of artifacts in a Maven repository.
    This just translates between a Maven coordinate
    and a URL in a Maven repository, and uses
    FsArtifactSource to open the file.

    For MavenArtifactSource, the artifact_id should
    be a Maven coordinate.
    """
    def __init__(self, base_uri: str):
        """
        :param base_uri:
            Base URI to a Maven repository.  Required.
            E.g. https://archiva.wikimedia.org/repository/mirror-maven-central
        """
        super().__init__(base_uri=base_uri)

    def url(self, artifact_id: str) -> str:
        """
        For MavenArtifactSource, the artifact_id is a Maven coordinate.

        :param artifact_id: A maven coordinate string.
        """

        # Make sure artifact_id looks like a coorindate.

        return maven_artifact_uri(artifact_id, self.base_uri)
